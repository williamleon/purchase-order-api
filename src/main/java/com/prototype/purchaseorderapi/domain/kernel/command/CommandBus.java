package com.prototype.purchaseorderapi.domain.kernel.command;

/**
 * Interface to define how to handle command executions {@link CommandExecute}
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface CommandBus {

	<R> R dispatch(Command command);
}
