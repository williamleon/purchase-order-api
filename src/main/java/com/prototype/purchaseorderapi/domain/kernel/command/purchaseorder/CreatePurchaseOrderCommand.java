package com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.purchaseorderapi.domain.kernel.command.Command;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.model.ProductOrder;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableCreatePurchaseOrderCommand.class, builder = ImmutableCreatePurchaseOrderCommand.Builder.class)
@JsonSerialize(as = ImmutableCreatePurchaseOrderCommand.class)
@Value.Immutable
public interface CreatePurchaseOrderCommand extends Command {

	String getUserEmail();

	List<ProductOrder> getProducts();

}
