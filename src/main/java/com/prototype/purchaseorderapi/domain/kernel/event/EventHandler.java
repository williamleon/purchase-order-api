package com.prototype.purchaseorderapi.domain.kernel.event;

/**
 * Interface to define how to handler events {@link Event}
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface EventHandler <T extends Event> {

	void handle(T event);
}
