package com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.purchaseorderapi.domain.kernel.command.Command;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableRevertOrderCommand.class)
@JsonSerialize(as = ImmutableRevertOrderCommand.class)
@Value.Immutable
public interface RevertOrderCommand extends Command {

	String getId();

	String getUserId();
}
