package com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutablePayment.class)
@JsonSerialize(as = ImmutablePayment.class)
@Value.Immutable
public interface Payment {

	String getOrderId();

	String getTransactionStatus();

	String getTransactionType();

	String getId();
}
