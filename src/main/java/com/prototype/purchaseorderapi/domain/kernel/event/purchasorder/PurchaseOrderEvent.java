package com.prototype.purchaseorderapi.domain.kernel.event.purchasorder;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.purchaseorderapi.domain.kernel.event.Event;
import com.prototype.purchaseorderapi.domain.kernel.model.PurchaseOrder;
import org.immutables.value.Value;

/**
 * Interface to identify product events
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutablePurchaseOrderEvent.class)
@JsonSerialize(as = ImmutablePurchaseOrderEvent.class)
public interface PurchaseOrderEvent extends Event {

	PurchaseOrder getOrder();
}
