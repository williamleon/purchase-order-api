package com.prototype.purchaseorderapi.domain.kernel.exception;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public class UserException extends RuntimeException {

	public UserException(String message) {

		super(message);
	}
}
