package com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.purchaseorderapi.domain.kernel.command.Command;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.model.Payment;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableUpdatePurchaseOrderByPaymentCommand.class)
@JsonSerialize(as = ImmutableUpdatePurchaseOrderByPaymentCommand.class)
@Value.Immutable
public interface UpdatePurchaseOrderByPaymentCommand extends Command {

	Payment getPayment();
}
