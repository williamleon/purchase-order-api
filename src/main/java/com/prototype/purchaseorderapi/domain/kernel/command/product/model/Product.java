package com.prototype.purchaseorderapi.domain.kernel.command.product.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableProduct.class, builder = ImmutableProduct.Builder.class)
@JsonSerialize(as = ImmutableProduct.class)
@Value.Immutable
public interface Product {

	String getId();

	String getReference();

	String getName();

	Double getPrice();

	Integer getAvailability();

}
