package com.prototype.purchaseorderapi.domain.kernel.command.product;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.purchaseorderapi.domain.kernel.command.Command;
import com.prototype.purchaseorderapi.domain.kernel.command.product.model.Product;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableUpdateProductCommand.class)
@JsonSerialize(as = ImmutableUpdateProductCommand.class)
@Value.Immutable
public interface UpdateProductCommand extends Command {

	Product getProduct();

}
