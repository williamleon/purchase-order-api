package com.prototype.purchaseorderapi.domain.kernel.model.mapper;

import lombok.NoArgsConstructor;

/**
 * Mapper util for business objects on business layer
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@NoArgsConstructor
public class Mapper {

}
