package com.prototype.purchaseorderapi.domain.kernel.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@Value.Immutable
@JsonDeserialize(as = ImmutablePurchaseOrder.class)
@JsonSerialize(as = ImmutablePurchaseOrder.class)
public interface PurchaseOrder {

	Optional<String> getId();

	Optional<Integer> getReference();

	String getUserId();

	@Value.Default
	default String getStatus() {

		return "CREATED";
	}

	Optional<LocalDateTime> getCreatedAt();

	List<Product> getProducts();

	@Value.Derived
	default Double getCost() {

		return getProducts().stream()
							.map(product -> product.getPrice() * product.getCant())
							.collect(Collectors.summingDouble(Double::doubleValue));
	}
}
