package com.prototype.purchaseorderapi.domain.kernel.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * Business object to handle product
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@Value.Immutable
@JsonDeserialize(as = ImmutableProduct.class)
@JsonSerialize(as = ImmutableProduct.class)
public interface Product {

	String getId();

	Integer getCant();

	Double getPrice();

}
