package com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableProductOrder.class, builder = ImmutableProductOrder.Builder.class)
@JsonSerialize(as = ImmutableProductOrder.class)
@Value.Immutable
public interface ProductOrder {

	String getId();

	Integer getCant();

}
