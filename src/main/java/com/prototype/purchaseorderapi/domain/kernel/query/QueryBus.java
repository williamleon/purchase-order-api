package com.prototype.purchaseorderapi.domain.kernel.query;

/**
 * Interface to define how to handle query executions {@link QueryHandler}
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface QueryBus {

	<R> R ask(Query query);
}
