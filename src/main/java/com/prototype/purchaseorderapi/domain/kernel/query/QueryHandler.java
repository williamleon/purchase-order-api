package com.prototype.purchaseorderapi.domain.kernel.query;

/**
 * Interface to define how to handle queries {@link Query}
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface QueryHandler<T extends Query, R> {

	/**
	 * Abstraction to define how to handle {@link Query}
	 *
	 * @param query
	 * @return
	 */
	R handle(T query);
}
