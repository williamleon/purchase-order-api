package com.prototype.purchaseorderapi.domain.kernel.event;

/**
 * Interface to define how to manage event handlers {@link EventHandler}
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface EventBus {

	void emit(Event event);
}
