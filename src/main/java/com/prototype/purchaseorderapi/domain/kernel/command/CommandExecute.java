package com.prototype.purchaseorderapi.domain.kernel.command;

/**
 * Interface to define how to execute commands {@link Command}
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface CommandExecute<T extends Command, R> {

	/**
	 * Abstraction to define how to handle {@link Command}
	 *
	 * @param command
	 * @return
	 */
	R execute(T command);
}
