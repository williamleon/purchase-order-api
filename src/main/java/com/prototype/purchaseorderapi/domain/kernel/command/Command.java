package com.prototype.purchaseorderapi.domain.kernel.command;

/**
 * Interface to define operation to be handle o execute
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface Command {

}
