package com.prototype.purchaseorderapi.domain.usecase;

import com.prototype.purchaseorderapi.domain.kernel.command.Command;
import com.prototype.purchaseorderapi.domain.kernel.command.CommandBus;
import com.prototype.purchaseorderapi.domain.kernel.command.CommandExecute;
import com.prototype.purchaseorderapi.domain.kernel.exception.MismatchConfigurationException;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Implementation of {@link CommandBus} to handle all command execution
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@RequiredArgsConstructor
public class LocalCommandBus implements CommandBus {

	private final Map<Object, CommandExecute> executions;

	/**
	 * {@inheritDoc}
	 */
	@Override public <R> R dispatch(final Command command) {

		return (R) executions.get(command.getClass().getName())
							 .peek(commandExecute -> log
									 .info(String.format("Preparing %s to dispatch", commandExecute.getClass().getName())))
							 .getOrElseThrow(() -> new MismatchConfigurationException(
									 String.format("Missing configuration for %s", command.getClass().getName())))
							 .execute(command);
	}
}
