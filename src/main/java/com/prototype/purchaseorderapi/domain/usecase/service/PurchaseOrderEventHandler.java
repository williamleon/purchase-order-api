package com.prototype.purchaseorderapi.domain.usecase.service;

import com.prototype.purchaseorderapi.domain.kernel.event.EventHandler;
import com.prototype.purchaseorderapi.domain.kernel.event.purchasorder.PurchaseOrderEvent;
import com.prototype.purchaseorderapi.domain.usecase.stream.PurchaseOrderBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PurchaseOrderEventHandler implements EventHandler<PurchaseOrderEvent> {

	private final PurchaseOrderBinding binding;

	@Override public void handle(final PurchaseOrderEvent event) {

		log.info(String.format("handling PurchaseOrderEvent for %s", event.getOrder().getId()));
		binding.send(event);
	}
}
