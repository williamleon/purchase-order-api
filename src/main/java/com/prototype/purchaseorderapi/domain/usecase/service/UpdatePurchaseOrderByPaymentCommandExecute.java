package com.prototype.purchaseorderapi.domain.usecase.service;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;
import static java.lang.String.format;

import com.prototype.purchaseorderapi.domain.kernel.command.CommandExecute;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.UpdatePurchaseOrderByPaymentCommand;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.model.Payment;
import com.prototype.purchaseorderapi.domain.kernel.event.EventBus;
import com.prototype.purchaseorderapi.domain.kernel.event.purchasorder.ImmutablePurchaseOrderEvent;
import com.prototype.purchaseorderapi.domain.kernel.exception.PurchaseOrderException;
import com.prototype.purchaseorderapi.domain.kernel.model.ImmutablePurchaseOrder;
import com.prototype.purchaseorderapi.domain.kernel.model.PurchaseOrder;
import com.prototype.purchaseorderapi.domain.usecase.repository.PurchaseOrderRepository;
import io.vavr.Tuple;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * CommandExecute to handle {@link UpdatePurchaseOrderByPaymentCommand}
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UpdatePurchaseOrderByPaymentCommandExecute implements CommandExecute<UpdatePurchaseOrderByPaymentCommand,
		Either<Throwable, PurchaseOrder>> {

	private static final String MESSAGE = "Getting error updating purchase order by payment transaction";
	private final PurchaseOrderRepository repository;
	private final EventBus eventBus;

	/**
	 * @param command
	 * @return
	 */
	@Override public Either<Throwable, PurchaseOrder> execute(final UpdatePurchaseOrderByPaymentCommand command) {

		log.info(format("Starting update purchase order [%s] base on payment transaction [%s][%s][%s]...",
						command.getPayment().getOrderId(), command.getPayment().getId(),
						command.getPayment().getTransactionType(), command.getPayment().getTransactionStatus()));

		return Try.of(command::getPayment)
				  .filter(payment -> payment.getTransactionType().equals("AUTHORIZATION_AND_CAPTURE"))
				  .transform(payments -> Try.of(() -> repository.findById(payments.get().getOrderId()).get())
											.map(po -> Tuple.of(payments.get(), po)))
				  .peek(tuple2 -> log.info(format("Found purchase order [%s][%s]", tuple2._2.getId(), tuple2._2.getStatus())))
				  .map(tuple2 -> changeState(tuple2._2, tuple2._1))
				  .peek(po -> log.info(format("Preparing purchase order [%s] to change status to [%s]", po.getId(), po.getStatus())))
				  .transform(pos -> Try.of(() -> repository.save(pos.get())))
				  .peek(po -> log.info(format("Update purchase order [%s] updated to [%s] status", po.getId(), po.getStatus())))
				  .peek(p -> eventBus.emit(ImmutablePurchaseOrderEvent.builder().order(p).build()))
				  .onFailure(throwable -> log.error(MESSAGE, throwable))
				  .toEither(() -> new PurchaseOrderException(MESSAGE));
	}

	/**
	 * Change de purchase order status depending on Payment transaction type and payment status
	 *
	 * @param purchaseOrder
	 * @param payment
	 * @return
	 */
	private PurchaseOrder changeState(PurchaseOrder purchaseOrder, Payment payment) {

		return Match(format("%s#%s", payment.getTransactionType(), payment.getTransactionStatus())).of(
				Case($("AUTHORIZATION_AND_CAPTURE#APPROVED"), ImmutablePurchaseOrder.copyOf(purchaseOrder)
																					.withStatus("COMPLETED")),
				Case($(), purchaseOrder));
	}
}
