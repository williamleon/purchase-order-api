package com.prototype.purchaseorderapi.domain.usecase.stream;

import com.prototype.purchaseorderapi.domain.kernel.event.purchasorder.PurchaseOrderEvent;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface PurchaseOrderBinding {

	void send(PurchaseOrderEvent productEvent);
}
