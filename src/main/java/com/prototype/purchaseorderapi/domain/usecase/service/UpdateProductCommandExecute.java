package com.prototype.purchaseorderapi.domain.usecase.service;

import com.prototype.purchaseorderapi.domain.kernel.command.CommandExecute;
import com.prototype.purchaseorderapi.domain.kernel.command.product.UpdateProductCommand;
import io.vavr.control.Either;
import io.vavr.control.Option;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface UpdateProductCommandExecute extends CommandExecute<UpdateProductCommand, Either<Throwable, Void>> {

}
