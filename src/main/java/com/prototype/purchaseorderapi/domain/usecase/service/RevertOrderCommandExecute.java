package com.prototype.purchaseorderapi.domain.usecase.service;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;

import java.util.List;

import com.prototype.purchaseorderapi.domain.kernel.command.CommandExecute;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.RevertOrderCommand;
import com.prototype.purchaseorderapi.domain.kernel.event.EventBus;
import com.prototype.purchaseorderapi.domain.kernel.event.purchasorder.ImmutablePurchaseOrderEvent;
import com.prototype.purchaseorderapi.domain.kernel.exception.PurchaseOrderException;
import com.prototype.purchaseorderapi.domain.kernel.model.ImmutablePurchaseOrder;
import com.prototype.purchaseorderapi.domain.kernel.model.PurchaseOrder;
import com.prototype.purchaseorderapi.domain.usecase.repository.PurchaseOrderRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RevertOrderCommandExecute implements CommandExecute<RevertOrderCommand, Either<Throwable, PurchaseOrder>> {

	private static final String MESSAGE = "Getting error reverting purchase order [%s] for user [%s]";
	private final PurchaseOrderRepository repository;
	private final List<String> STATUS_TO_REVERT = List.of("CREATED", "COMPLETED");
	private final EventBus eventBus;

	/**
	 * Abstraction to define how to handle {@link Command}
	 *
	 * @param command
	 * @return
	 */
	@Override public Either<Throwable, PurchaseOrder> execute(final RevertOrderCommand command) {

		log.info(String.format("Starting revert purchase order [%s] for user [%s]...", command.getId(), command.getUserId()));
		return Try.of(() -> repository.findByIdAndUserId(command.getId(), command.getUserId()).get())
				  .filter(p -> STATUS_TO_REVERT.contains(p.getStatus()))
				  .peek(po -> log.info(String.format("Found purchase order [%s] for user [%s]", po.getId(), po.getUserId())))
				  .map(this::changeState)
				  .transform(pos -> Try.of(() -> repository.save(pos.get())))
				  .peek(po -> log.info(String.format("Update purchase order [%s] updated to [%s] status", po.getId(), po.getStatus())))
				  .peek(p -> eventBus.emit(ImmutablePurchaseOrderEvent.builder().order(p).build()))
				  .onFailure(throwable -> log.error(String.format(MESSAGE, command.getId(), command.getUserId()), throwable))
				  .toEither(() -> new PurchaseOrderException(String.format(MESSAGE, command.getId(),"")));
	}

	private PurchaseOrder changeState(PurchaseOrder purchaseOrder) {

		return Match(purchaseOrder.getStatus()).of(
				Case($("CREATED"), ImmutablePurchaseOrder.copyOf(purchaseOrder).withStatus("CANCELED")),
				Case($("COMPLETED"), ImmutablePurchaseOrder.copyOf(purchaseOrder).withStatus("REVERSED")),
				Case($(), purchaseOrder));
	}
}
