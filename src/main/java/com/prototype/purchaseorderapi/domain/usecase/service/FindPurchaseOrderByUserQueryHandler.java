package com.prototype.purchaseorderapi.domain.usecase.service;

import java.util.List;

import com.prototype.purchaseorderapi.domain.kernel.exception.PurchaseOrderException;
import com.prototype.purchaseorderapi.domain.kernel.model.PurchaseOrder;
import com.prototype.purchaseorderapi.domain.kernel.query.QueryHandler;
import com.prototype.purchaseorderapi.domain.kernel.query.purchaseorder.FindPurchaseOrderByUserQuery;
import com.prototype.purchaseorderapi.domain.usecase.repository.PurchaseOrderRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class FindPurchaseOrderByUserQueryHandler implements QueryHandler<FindPurchaseOrderByUserQuery, Either<Throwable,
		List<PurchaseOrder>>> {

	private static final String MESSAGE = "Getting error finding purchase order by user";
	private final PurchaseOrderRepository purchaseOrderRepository;

	/**
	 * Find all purchase order related to the user
	 *
	 * @param query
	 * @return
	 */
	@Override public Either<Throwable, List<PurchaseOrder>> handle(final FindPurchaseOrderByUserQuery query) {

		log.info(String.format("Starting finding purchase order by user [%s]...", query.getUserId()));
		return Try.of(() -> purchaseOrderRepository.findAllByUserId(query.getUserId()))
				  .peek(l -> log.info(String.format("Found [%s] purchase orders for user [%s]", l.size(), query.getUserId())))
				  .onFailure(throwable -> log.error(MESSAGE, throwable))
				  .toEither(() -> new PurchaseOrderException(MESSAGE));
	}
}
