package com.prototype.purchaseorderapi.domain.usecase;

import com.prototype.purchaseorderapi.domain.kernel.exception.MismatchConfigurationException;
import com.prototype.purchaseorderapi.domain.kernel.query.Query;
import com.prototype.purchaseorderapi.domain.kernel.query.QueryBus;
import com.prototype.purchaseorderapi.domain.kernel.query.QueryHandler;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Implementation of {@link QueryBus} to handle all query execution
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@RequiredArgsConstructor
public class LocalQueryBus implements QueryBus {

	private final Map<Object, QueryHandler> handlerMap;

	/**
	 * {@inheritDoc}
	 */
	@Override public <R> R ask(final Query query) {

		return (R) handlerMap.get(query.getClass().getName())
							 .peek(commandExecute -> log
									 .info(String.format("Preparing %s to ask", commandExecute.getClass().getName())))
							 .getOrElseThrow(() -> new MismatchConfigurationException(
									 String.format("Missing configuration for %s", query.getClass().getName())))
							 .handle(query);
	}
}
