package com.prototype.purchaseorderapi.domain.usecase.service;

import com.prototype.purchaseorderapi.domain.kernel.command.CommandExecute;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.CreatePurchaseOrderCommand;
import com.prototype.purchaseorderapi.domain.kernel.model.PurchaseOrder;
import io.vavr.control.Either;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface CreatePurchaseOrderCommandExecute extends CommandExecute<CreatePurchaseOrderCommand, Either<Throwable, PurchaseOrder>> {

}
