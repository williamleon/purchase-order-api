package com.prototype.purchaseorderapi.domain.usecase;

import com.prototype.purchaseorderapi.domain.kernel.event.Event;
import com.prototype.purchaseorderapi.domain.kernel.event.EventBus;
import com.prototype.purchaseorderapi.domain.kernel.event.EventHandler;
import com.prototype.purchaseorderapi.domain.kernel.exception.MismatchConfigurationException;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Implementation of {@link EventBus} to handle all event handlers
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@RequiredArgsConstructor
public class LocalEventBus implements EventBus {

	private final Map<Object, EventHandler<Event>> handlerMap;

	/**
	 * {@inheritDoc}
	 */
	@Override public void emit(final Event event) {

		handlerMap.get(event.getClass().getName())
				  .peek(commandExecute -> log.info(String.format("Preparing %s to emit", commandExecute.getClass().getName())))
				  .getOrElseThrow(() -> new MismatchConfigurationException(
						  String.format("Missing configuration for %s", event.getClass().getName())))
				  .handle(event);
	}
}
