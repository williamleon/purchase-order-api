package com.prototype.purchaseorderapi.domain.usecase.repository;

import java.util.List;
import java.util.Optional;

import com.prototype.purchaseorderapi.domain.kernel.model.PurchaseOrder;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface PurchaseOrderRepository {

	List<PurchaseOrder> findAllByUserId(String userId);

	Optional<PurchaseOrder> findByIdAndUserId(String id, String userId);

	PurchaseOrder save(PurchaseOrder purchaseOrder);

	Optional<PurchaseOrder> findById(String id);

}
