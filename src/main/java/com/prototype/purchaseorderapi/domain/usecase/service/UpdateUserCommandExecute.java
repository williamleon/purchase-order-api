package com.prototype.purchaseorderapi.domain.usecase.service;

import com.prototype.purchaseorderapi.domain.kernel.command.CommandExecute;
import com.prototype.purchaseorderapi.domain.kernel.command.user.UpdateUserCommand;
import io.vavr.control.Either;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface UpdateUserCommandExecute extends CommandExecute<UpdateUserCommand, Either<Throwable, Void>> {

}
