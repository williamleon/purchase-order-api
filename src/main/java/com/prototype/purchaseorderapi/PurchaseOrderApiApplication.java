package com.prototype.purchaseorderapi;

import com.prototype.purchaseorderapi.infrastructure.adapter.inbound.stream.InputBinding;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.stream.OutputBinding;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@EnableBinding({OutputBinding.class, InputBinding.class})
@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class PurchaseOrderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PurchaseOrderApiApplication.class, args);
	}

}
