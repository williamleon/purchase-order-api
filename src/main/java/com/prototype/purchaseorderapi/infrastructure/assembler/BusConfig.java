package com.prototype.purchaseorderapi.infrastructure.assembler;

import com.prototype.purchaseorderapi.domain.kernel.command.CommandExecute;

import com.prototype.purchaseorderapi.domain.kernel.command.product.ImmutableUpdateProductCommand;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.ImmutableCreatePurchaseOrderCommand;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.ImmutableRevertOrderCommand;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.ImmutableUpdatePurchaseOrderByPaymentCommand;
import com.prototype.purchaseorderapi.domain.kernel.command.user.ImmutableUpdateUserCommand;
import com.prototype.purchaseorderapi.domain.kernel.event.EventHandler;

import com.prototype.purchaseorderapi.domain.kernel.event.purchasorder.ImmutablePurchaseOrderEvent;
import com.prototype.purchaseorderapi.domain.kernel.query.QueryHandler;
import com.prototype.purchaseorderapi.domain.kernel.query.purchaseorder.ImmutableFindPurchaseOrderByUserQuery;
import com.prototype.purchaseorderapi.domain.usecase.LocalCommandBus;
import com.prototype.purchaseorderapi.domain.usecase.LocalEventBus;
import com.prototype.purchaseorderapi.domain.usecase.LocalQueryBus;
import io.vavr.collection.HashMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure Query/Command bus
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Configuration
public class BusConfig {

	@Bean
	public LocalCommandBus commandBus(CommandExecute updateProductCommandExecuteImpl,
									  CommandExecute updateUserCommandExecuteImpl,
									  CommandExecute createPurchaseOrderCommandExecuteImpl,
									  CommandExecute revertOrderCommandExecute,
									  CommandExecute updatePurchaseOrderByPaymentCommandExecute) {

		return new LocalCommandBus(HashMap.of(ImmutableUpdateProductCommand.class.getName(), updateProductCommandExecuteImpl,
											   ImmutableUpdateUserCommand.class.getName(), updateUserCommandExecuteImpl,
											   ImmutableCreatePurchaseOrderCommand.class.getName(), createPurchaseOrderCommandExecuteImpl,
											   ImmutableRevertOrderCommand.class.getName(), revertOrderCommandExecute,
											   ImmutableUpdatePurchaseOrderByPaymentCommand.class.getName(),
											   updatePurchaseOrderByPaymentCommandExecute));
	}

	@Bean
	public LocalQueryBus queryBus(QueryHandler findPurchaseOrderByUserQueryHandler) {

		return new LocalQueryBus(HashMap.of(ImmutableFindPurchaseOrderByUserQuery.class.getName(), findPurchaseOrderByUserQueryHandler));
	}

	@Bean
	public LocalEventBus eventBus(EventHandler purchaseOrderEventHandler) {

		return new LocalEventBus(HashMap.of(ImmutablePurchaseOrderEvent.class.getName(), purchaseOrderEventHandler));
	}
}
