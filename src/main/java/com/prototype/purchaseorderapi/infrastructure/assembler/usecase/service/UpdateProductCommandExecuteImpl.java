package com.prototype.purchaseorderapi.infrastructure.assembler.usecase.service;

import java.util.function.Function;

import com.prototype.purchaseorderapi.domain.kernel.command.product.UpdateProductCommand;
import com.prototype.purchaseorderapi.domain.kernel.command.product.model.Product;
import com.prototype.purchaseorderapi.domain.kernel.exception.ProductException;
import com.prototype.purchaseorderapi.domain.usecase.service.UpdateProductCommandExecute;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model.DBProduct;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.repository.DBProductRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UpdateProductCommandExecuteImpl implements UpdateProductCommandExecute {

	private static final String MESSAGE = "Getting error updating product";
	private final DBProductRepository dbProductRepository;

	/**
	 * Save the product information that is received in command
	 *
	 * @param command {@link UpdateProductCommand}
	 * @return {@link Either}
	 */
	@Override public Either<Throwable, Void> execute(final UpdateProductCommand command) {

		return Try.of(command::getProduct)
				  .peek(p -> log.info(String.format("Starting update product command for [%s]...", p.getId())))
				  .map(fromProduct)
				  .transform(dbProducts -> Try.of(() -> dbProductRepository.save(dbProducts.get())))
				  .peek(p -> log.info(String.format("Product [%s] updated successfully", p.getId())))
				  .map(product -> (Void) null)
				  .onFailure(throwable -> log.error(MESSAGE, throwable))
				  .toEither(() -> new ProductException(MESSAGE));
	}

	/**
	 * Function to map {@link Product} inside command to {@link DBProduct}
	 */
	private static Function<Product, DBProduct> fromProduct = p -> DBProduct.builder()
																			.id(p.getId())
																			.availability(p.getAvailability())
																			.name(p.getName())
																			.price(p.getPrice())
																			.reference(p.getReference())
																			.build();
}
