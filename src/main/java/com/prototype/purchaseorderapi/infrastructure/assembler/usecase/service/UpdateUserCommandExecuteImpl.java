package com.prototype.purchaseorderapi.infrastructure.assembler.usecase.service;

import static com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.mapper.UserMapper.fromUser;

import com.prototype.purchaseorderapi.domain.kernel.command.user.UpdateUserCommand;
import com.prototype.purchaseorderapi.domain.kernel.exception.ProductException;
import com.prototype.purchaseorderapi.domain.usecase.service.UpdateUserCommandExecute;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.repository.DBUserRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UpdateUserCommandExecuteImpl implements UpdateUserCommandExecute {

	private static final String MESSAGE = "Getting error updating user";
	private final DBUserRepository dbUserRepository;

	/**
	 * Save the user information that is received in command
	 *
	 * @param command {@link UpdateUserCommand}
	 * @return {@link Either}
	 */
	@Override public Either<Throwable, Void> execute(final UpdateUserCommand command) {

		return Try.of(command::getUser)
				  .peek(u -> log.info(String.format("Starting update user command for [%s]...", u.getId())))
				  .map(fromUser)
				  .transform(dbProducts -> Try.of(() -> dbUserRepository.save(dbProducts.get())))
				  .peek(u -> log.info(String.format("User [%s] updated successfully", u.getId())))
				  .map(u -> (Void) null)
				  .onFailure(throwable -> log.error(MESSAGE, throwable))
				  .toEither(() -> new ProductException(MESSAGE));
	}

}
