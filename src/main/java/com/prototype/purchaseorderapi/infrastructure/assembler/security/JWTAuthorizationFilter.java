package com.prototype.purchaseorderapi.infrastructure.assembler.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Filter implementation to handler JWT authorization
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public class JWTAuthorizationFilter extends OncePerRequestFilter {

	private String HEADER = "Authorization";
	private String PREFIX = "Bearer ";
	private String SECRET = "74aFWrRSneNDy5St";

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {

		try {
			if (request.getRequestURI().contains("/admin/purchase-order")) {
				if (existJWTToken(request, response)) {
					Claims claims = validateToken(request);
					if (Objects.nonNull(claims.get("authorities"))) {
						setUpSpringAuthentication(claims);
					} else {
						SecurityContextHolder.clearContext();
					}
					chain.doFilter(request, response);
				} else {
					response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token needed");
					return;
				}
			} else {
				chain.doFilter(request, response);
			}

		} catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException e) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
			return;
		}
	}

	private boolean existJWTToken(HttpServletRequest request, HttpServletResponse res) {

		String authenticationHeader = request.getHeader(HEADER);
		if (authenticationHeader == null || !authenticationHeader.startsWith(PREFIX)) {
			return false;
		}
		return true;
	}

	private Claims validateToken(HttpServletRequest request) {

		return Jwts
				.parser()
				.setSigningKey(SECRET)
				.parseClaimsJws(request.getHeader(HEADER).replace(PREFIX, ""))
				.getBody();
	}

	private void setUpSpringAuthentication(Claims claims) {

		List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
		for (String authority : ((String) claims.get("authorities")).split(",")) {
			authorityList.add(new SimpleGrantedAuthority(authority));
		}
		SecurityContextHolder.getContext()
							 .setAuthentication(new UsernamePasswordAuthenticationToken(claims.getSubject(), null, authorityList));

	}

}
