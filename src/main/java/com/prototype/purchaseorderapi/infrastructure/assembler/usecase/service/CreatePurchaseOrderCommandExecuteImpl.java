package com.prototype.purchaseorderapi.infrastructure.assembler.usecase.service;

import static com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.mapper.PurchaseOrderMapper.fromCommandAndDBUserAndDBProducts;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.CreatePurchaseOrderCommand;

import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.model.ProductOrder;
import com.prototype.purchaseorderapi.domain.kernel.event.EventBus;
import com.prototype.purchaseorderapi.domain.kernel.event.purchasorder.ImmutablePurchaseOrderEvent;
import com.prototype.purchaseorderapi.domain.kernel.exception.ProductException;
import com.prototype.purchaseorderapi.domain.kernel.exception.PurchaseOrderException;
import com.prototype.purchaseorderapi.domain.kernel.exception.UserException;
import com.prototype.purchaseorderapi.domain.kernel.model.PurchaseOrder;
import com.prototype.purchaseorderapi.domain.usecase.service.CreatePurchaseOrderCommandExecute;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.mapper.PurchaseOrderMapper;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model.DBProduct;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model.DBUser;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.repository.DBProductRepository;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.repository.DBPurchaseOrderRepository;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.repository.DBUserRepository;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.Tuple3;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CreatePurchaseOrderCommandExecuteImpl implements CreatePurchaseOrderCommandExecute {

	private static final String MESSAGE = "Getting error creating purchase order";
	private static final String PRODUCT_MESSAGE = "Error validating Availability for [%s]";
	private static final String USER_MESSAGE = "Error validating User [%s]";
	private final EventBus eventBus;
	private final DBUserRepository userRepository;
	private final DBProductRepository productRepository;
	private final DBPurchaseOrderRepository purchaseOrderRepository;

	/**
	 *
	 *
	 * @param command {@link CreatePurchaseOrderCommand}
	 * @return {@link Either}
	 */
	@Override public Either<Throwable, PurchaseOrder> execute(final CreatePurchaseOrderCommand command) {

		log.info("Starting creating purchase order ...");
		return Try.of(() -> command)
				  .peek(c -> log.info(String.format("Validating if user [%s] exist", c.getUserEmail())))
				  .transform(commands -> validateUser(commands.get()))
				  .peek(t -> log.info("Validating products availability "))
				  .transform(tuple2s -> validateProducts(tuple2s.get()))
				  .peek(t -> log.info("Creating Purchase order in system"))
				  .transform(t3s -> Try.of(() -> purchaseOrderRepository.save(fromCommandAndDBUserAndDBProducts(t3s.get()._1,
																												t3s.get()._2,
																												t3s.get()._3))))
				  .peek(p -> log.info(String.format("Purchase order successfully created id: [%s]", p.getId())))
				  .map(PurchaseOrderMapper::fromDBPurchaseOrder)
				  .peek(p -> eventBus.emit(ImmutablePurchaseOrderEvent.builder().order(p).build()))
				  .onFailure(throwable -> log.error(MESSAGE, throwable))
				  .toEither(() -> new PurchaseOrderException(MESSAGE));
	}

	/**
	 * Validate if given user in command exist in system
	 *
	 * @param command {@link CreatePurchaseOrderCommand}
	 * @return {@link Tuple2} with values of command and user system information
	 */
	private Try<Tuple2<CreatePurchaseOrderCommand, DBUser>> validateUser(CreatePurchaseOrderCommand command) {

		return Try.of(() -> userRepository.findByEmail(command.getUserEmail())
										  .orElseThrow(() -> new UserException(String.format(USER_MESSAGE, command.getUserEmail()))))
				  .map(u -> Tuple.of(command, u));
	}

	/**
	 * Validate if product in command operation exist in system and also validates if each product has availability
	 *
	 * @param tuple {@link Tuple2} with values of command and user system information
	 * @return {@link Try} with values of command and user system information
	 */
	private Try<Tuple3<CreatePurchaseOrderCommand, DBUser, List<DBProduct>>> validateProducts(
			Tuple2<CreatePurchaseOrderCommand, DBUser> tuple) {

		return Try.of(() -> productRepository.findAllById(getProductIdListFromCommand(tuple._1)))
				  .transform(lists -> Try.of(() -> validateProducts(tuple._1, lists.get())))
				  .map(products -> Tuple.of(tuple._1, tuple._2, products));
	}

	/**
	 * Validate if product in command operation exist in system and also validates if each product has availability
	 *
	 * @param command        {@link CreatePurchaseOrderCommand}
	 * @param systemProducts {@link List<DBProduct>}
	 * @return {@link List<DBProduct>}
	 */
	private List<DBProduct> validateProducts(CreatePurchaseOrderCommand command, List<DBProduct> systemProducts) {

		command.getProducts()
			   .forEach(productOrder -> Optional.of(productOrder)
												.filter(p -> checkAvailability(p, systemProducts))
												.orElseThrow(() -> new ProductException(
														String.format(PRODUCT_MESSAGE, productOrder.getId()))));

		return systemProducts;
	}

	/**
	 * Evaluate if an specific product has availability
	 *
	 * @param product        {@link ProductOrder} product in system
	 * @param systemProducts {@link List<DBProduct>}
	 * @return boolean with the evaluation result
	 */
	private boolean checkAvailability(ProductOrder product, List<DBProduct> systemProducts) {

		return product.getCant() <= systemProducts.stream()
												  .filter(p -> p.getId().equals(product.getId()))
												  .findFirst()
												  .orElseThrow(() -> new ProductException(String.format(PRODUCT_MESSAGE, product.getId())))
												  .getAvailability();
	}

	/**
	 * Retrieve all product id from command {@link CreatePurchaseOrderCommand}
	 *
	 * @param command {@link CreatePurchaseOrderCommand}
	 * @return {@link List} of product id
	 */
	private List<String> getProductIdListFromCommand(CreatePurchaseOrderCommand command) {

		return command.getProducts().stream().map(ProductOrder::getId).collect(Collectors.toList());
	}
}
