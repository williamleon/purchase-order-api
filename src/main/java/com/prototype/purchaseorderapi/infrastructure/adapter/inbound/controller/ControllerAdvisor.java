package com.prototype.purchaseorderapi.infrastructure.adapter.inbound.controller;

import static com.prototype.purchaseorderapi.infrastructure.adapter.inbound.controller.ResponseUtil.getMessage;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * + Handler exception for
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleException(
			Exception ex) {

		return getObjectResponseEntity(ex);
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<Object> handleRuntimeException(
			RuntimeException ex) {

		return getObjectResponseEntity(ex);
	}

	@ExceptionHandler(Throwable.class)
	public ResponseEntity<Object> handleThrowable(
			Throwable ex) {

		return getObjectResponseEntity(ex);
	}

	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<Object> handleAuthorizationFoundException(
			AccessDeniedException ex, WebRequest request) {

		return new ResponseEntity<>(getMessage(ex.getMessage()), HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler({AuthenticationException.class})
	public ResponseEntity<Object> handleAuthenticationException(Exception ex) {

		return new ResponseEntity<>(getMessage(ex.getMessage()), HttpStatus.UNAUTHORIZED);
	}

	private ResponseEntity<Object> getObjectResponseEntity(Throwable ex) {

		log.error("Generic Error", ex);

		return new ResponseEntity<>(getMessage("Generic error"), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
