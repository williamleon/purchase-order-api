package com.prototype.purchaseorderapi.infrastructure.adapter.inbound.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface InputBinding {

	String PRODUCT_MESSAGE = "product";
	String USER_MESSAGE = "user";
	String PAYMENT_MESSAGE = "payment";

	@Input(PRODUCT_MESSAGE)
	SubscribableChannel productChannel();

	@Input(USER_MESSAGE)
	SubscribableChannel userChannel();

	@Input(PAYMENT_MESSAGE)
	SubscribableChannel paymentChannel();
}
