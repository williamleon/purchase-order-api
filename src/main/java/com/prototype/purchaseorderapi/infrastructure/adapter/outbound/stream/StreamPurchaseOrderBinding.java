package com.prototype.purchaseorderapi.infrastructure.adapter.outbound.stream;

import com.prototype.purchaseorderapi.domain.kernel.event.purchasorder.PurchaseOrderEvent;
import com.prototype.purchaseorderapi.domain.usecase.stream.PurchaseOrderBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class StreamPurchaseOrderBinding implements PurchaseOrderBinding {

	private final OutputBinding outputBinding;

	@Override public void send(final PurchaseOrderEvent purchaseOrderEvent) {
		log.info(String.format("Sending event for purchase order %s", purchaseOrderEvent.getOrder().getId()));
		outputBinding.purchaseOrderChannel()
					 .send(MessageBuilder
								   .withPayload(purchaseOrderEvent)
								   .build());
	}
}
