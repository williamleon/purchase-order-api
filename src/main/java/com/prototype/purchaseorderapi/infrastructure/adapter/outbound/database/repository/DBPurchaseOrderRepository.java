package com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.repository;

import java.util.List;
import java.util.Optional;

import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model.DBPurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Repository
public interface DBPurchaseOrderRepository extends JpaRepository<DBPurchaseOrder, String> {

	List<DBPurchaseOrder> findAllByUserId(String userId);

	Optional<DBPurchaseOrder> findByIdAndUserId(String id, String userId);
}
