package com.prototype.purchaseorderapi.infrastructure.adapter.inbound.controller;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import lombok.NoArgsConstructor;

/**
 * Util class for support common operations in controller layer
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@NoArgsConstructor
public class ResponseUtil {

	/**
	 * Create standard message for errors
	 *
	 * @param message
	 * @return
	 */
	public static final Map getMessage(String message) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", message);
		return body;
	}

}
