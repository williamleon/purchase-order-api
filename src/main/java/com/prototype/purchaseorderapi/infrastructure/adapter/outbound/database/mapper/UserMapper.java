package com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.mapper;

import java.util.function.Function;

import com.prototype.purchaseorderapi.domain.kernel.command.user.model.User;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model.DBUser;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public class UserMapper {

	/**
	 * Function to map {@link User} inside command to {@link DBUser}
	 */
	public static Function<User, DBUser> fromUser = u -> DBUser.builder()
																.id(u.getId())
																.email(u.getEmail())
																.build();
}
