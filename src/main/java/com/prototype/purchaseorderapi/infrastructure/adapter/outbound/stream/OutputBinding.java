package com.prototype.purchaseorderapi.infrastructure.adapter.outbound.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */

public interface OutputBinding {

	@Output("purchase-order")
	MessageChannel purchaseOrderChannel();
}
