package com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Data
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "purchase_orders", uniqueConstraints = {
		@UniqueConstraint(columnNames = "reference")
})
public class DBPurchaseOrder {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String id;

	@Generated(GenerationTime.INSERT)
	@Column(nullable = false)
	private Integer reference;

	@Column(nullable = false)
	private String userId;
	@Column(nullable = false)
	private String status;

	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "purchaseOrder", orphanRemoval = true, cascade= CascadeType.PERSIST)
	private List<DBProductOrder> products;
}
