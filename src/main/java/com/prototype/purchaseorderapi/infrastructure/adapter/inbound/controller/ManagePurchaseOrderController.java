package com.prototype.purchaseorderapi.infrastructure.adapter.inbound.controller;

import java.util.List;

import com.prototype.purchaseorderapi.domain.kernel.command.CommandBus;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.ImmutableRevertOrderCommand;
import com.prototype.purchaseorderapi.domain.kernel.model.PurchaseOrder;
import com.prototype.purchaseorderapi.domain.kernel.query.QueryBus;
import com.prototype.purchaseorderapi.domain.kernel.query.purchaseorder.ImmutableFindPurchaseOrderByUserQuery;
import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("admin/purchase-order")
public class ManagePurchaseOrderController {

	private final CommandBus commandBus;

	private final QueryBus queryBus;

	@PostMapping("/{purchaseOrderId}")
	public ResponseEntity<?> revert(@PathVariable String purchaseOrderId) {

		String userId = SecurityContextHolder.getContext().getAuthentication().getName();
		Either<Throwable, PurchaseOrder> execution = commandBus
				.dispatch(ImmutableRevertOrderCommand.builder()
													 .id(purchaseOrderId)
													 .userId(userId)
													 .build());
		return execution.map(ResponseEntity::ok)
						.getOrElseGet(
								throwable -> new ResponseEntity(ResponseUtil.getMessage(throwable.getMessage()), HttpStatus.CONFLICT));

	}

	@GetMapping
	public ResponseEntity<?> getByUser() {

		String userId = SecurityContextHolder.getContext().getAuthentication().getName();
		Either<Throwable, List<PurchaseOrder>> execution = queryBus
				.ask(ImmutableFindPurchaseOrderByUserQuery.builder().userId(userId).build());
		return execution.map(ResponseEntity::ok)
						.getOrElseGet(
								throwable -> new ResponseEntity(ResponseUtil.getMessage(throwable.getMessage()), HttpStatus.UNAUTHORIZED));
	}
}
