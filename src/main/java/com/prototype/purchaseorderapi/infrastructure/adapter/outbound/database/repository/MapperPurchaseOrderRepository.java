package com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.repository;

import static com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.mapper.PurchaseOrderMapper.fromDBPurchaseOrder;
import static com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.mapper.PurchaseOrderMapper.fromPurchaseOrder;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.prototype.purchaseorderapi.domain.kernel.model.PurchaseOrder;
import com.prototype.purchaseorderapi.domain.usecase.repository.PurchaseOrderRepository;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.mapper.PurchaseOrderMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class MapperPurchaseOrderRepository implements PurchaseOrderRepository {

	private final DBPurchaseOrderRepository repository;

	@Override public List<PurchaseOrder> findAllByUserId(final String userId) {

		return repository.findAllByUserId(userId).stream()
						 .map(PurchaseOrderMapper::fromDBPurchaseOrder)
						 .collect(Collectors.toList());
	}

	@Override public Optional<PurchaseOrder> findByIdAndUserId(final String id, final String userId) {

		return repository.findByIdAndUserId(id, userId).map(PurchaseOrderMapper::fromDBPurchaseOrder);
	}

	@Override public PurchaseOrder save(final PurchaseOrder purchaseOrder) {

		return fromDBPurchaseOrder(repository.save(fromPurchaseOrder(purchaseOrder, repository.findById(purchaseOrder.getId().get()).get())));
	}

	@Override public Optional<PurchaseOrder> findById(final String id) {

		return repository.findById(id).map(PurchaseOrderMapper::fromDBPurchaseOrder);
	}
}
