package com.prototype.purchaseorderapi.infrastructure.adapter.inbound.stream.listener;

import com.prototype.purchaseorderapi.domain.kernel.command.CommandBus;
import com.prototype.purchaseorderapi.domain.kernel.command.product.UpdateProductCommand;
import com.prototype.purchaseorderapi.infrastructure.adapter.inbound.stream.InputBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ProductListener {

	public final CommandBus bus;

	@StreamListener(target = InputBinding.PRODUCT_MESSAGE)
	public void updateProductAvailability(UpdateProductCommand command) {

		log.info(String.format("Listen product event for %s", command.getProduct().getId()));
		bus.dispatch(command);
	}
}
