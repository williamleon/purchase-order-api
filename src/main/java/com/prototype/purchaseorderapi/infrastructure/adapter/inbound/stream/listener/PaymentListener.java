package com.prototype.purchaseorderapi.infrastructure.adapter.inbound.stream.listener;

import com.prototype.purchaseorderapi.domain.kernel.command.CommandBus;
import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.UpdatePurchaseOrderByPaymentCommand;
import com.prototype.purchaseorderapi.infrastructure.adapter.inbound.stream.InputBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

/**
 * Listener for payment event
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class PaymentListener {

	public final CommandBus bus;

	@StreamListener(target = InputBinding.PAYMENT_MESSAGE)
	public void updatePurchaseOrder(UpdatePurchaseOrderByPaymentCommand command) {

		log.info(String.format("Listen payment event for %s", command.getPayment().getId()));
		bus.dispatch(command);
	}
}
