package com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.mapper;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.prototype.purchaseorderapi.domain.kernel.command.purchaseorder.CreatePurchaseOrderCommand;
import com.prototype.purchaseorderapi.domain.kernel.model.ImmutableProduct;
import com.prototype.purchaseorderapi.domain.kernel.model.ImmutablePurchaseOrder;
import com.prototype.purchaseorderapi.domain.kernel.model.PurchaseOrder;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model.DBProduct;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model.DBProductOrder;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model.DBPurchaseOrder;
import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model.DBUser;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public class PurchaseOrderMapper {

	public static final DBPurchaseOrder fromCommandAndDBUserAndDBProducts(CreatePurchaseOrderCommand command, DBUser user,
																		  List<DBProduct> products) {

		final DBPurchaseOrder purchaseOrder = DBPurchaseOrder.builder()
															 .userId(user.getId())
															 .status("CREATED")
															 .updatedAt(LocalDateTime.now())
															 .createdAt(LocalDateTime.now())
															 .products(command.getProducts().stream()
																			  .map(po -> DBProductOrder.builder()
																									   .cant(po.getCant())
																									   .productId(po.getId())
																									   .price(products.stream()
																													  .filter(p -> po
																															  .getId()
																															  .equals(p.getId()))
																													  .map(DBProduct::getPrice)
																													  .findFirst()
																													  .orElse(Double.NaN))
																									   .build())
																			  .collect(Collectors.toList()))
															 .build();
		purchaseOrder.getProducts().forEach(dbProductOrder -> dbProductOrder.setPurchaseOrder(purchaseOrder));
		return purchaseOrder;
	}

	public static final PurchaseOrder fromDBPurchaseOrder(DBPurchaseOrder dbPurchaseOrder) {

		return ImmutablePurchaseOrder.builder()
									 .id(dbPurchaseOrder.getId())
									 .userId(dbPurchaseOrder.getUserId())
									 .reference(dbPurchaseOrder.getReference())
									 .status(dbPurchaseOrder.getStatus())
									 .createdAt(dbPurchaseOrder.getCreatedAt())
									 .products(dbPurchaseOrder
													   .getProducts().stream()
													   .map(dbProductOrder -> ImmutableProduct.builder()
																							  .id(dbProductOrder.getProductId())
																							  .cant(dbProductOrder.getCant())
																							  .price(dbProductOrder.getPrice())
																							  .build())
													   .collect(Collectors.toList()))
									 .build();
	}

	public static final DBPurchaseOrder fromPurchaseOrder(PurchaseOrder purchaseOrder, DBPurchaseOrder origin) {

		return DBPurchaseOrder.builder()
							  .id(purchaseOrder.getId().orElse(null))
							  .reference(purchaseOrder.getReference().orElse(null))
							  .userId(purchaseOrder.getUserId())
							  .status(purchaseOrder.getStatus())
							  .createdAt(purchaseOrder.getCreatedAt().get())
							  .updatedAt(LocalDateTime.now())
							  .products(origin.getProducts())
							  .build();
	}
}
