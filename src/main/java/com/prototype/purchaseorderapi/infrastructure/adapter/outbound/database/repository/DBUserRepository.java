package com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.repository;

import java.util.Optional;

import com.prototype.purchaseorderapi.infrastructure.adapter.outbound.database.model.DBUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Repository
public interface DBUserRepository extends JpaRepository<DBUser, String> {

	Optional<DBUser> findByEmail(String email);
}
